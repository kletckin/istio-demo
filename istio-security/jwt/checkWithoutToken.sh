#! /bin/sh
TOKEN=$(curl https://raw.githubusercontent.com/istio/istio/release-1.4/security/tools/jwt/samples/groups-scope.jwt -s)
oc exec $(oc get pod -l app=sleep -n jwt -o jsonpath={.items..metadata.name}) -c sleep -n jwt -- curl http://httpbin.jwt:8000/ip -s -o /dev/null -w "%{http_code}\n"