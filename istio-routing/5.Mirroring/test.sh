#! /bin/sh
SLEEP_POD=$(oc get pod -l app=sleep -o jsonpath={.items..metadata.name})
### run ping from sleep pod
oc  exec -it $SLEEP_POD -c sleep -- sh -c 'curl  http://httpbin:8000/headers' 