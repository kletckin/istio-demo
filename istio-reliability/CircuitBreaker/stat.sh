#! /bin/sh
FORTIO_POD=$(oc get pod | grep fortio | awk '{ print $1 }')
oc exec $FORTIO_POD -c istio-proxy -- pilot-agent request GET stats | grep httpbin | grep pending
