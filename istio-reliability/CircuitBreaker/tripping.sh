#! /bin/sh
FORTIO_POD=$(oc get pod | grep fortio | awk '{ print $1 }')
Connections=$1
Count=$2
oc exec -it $FORTIO_POD  -c fortio /usr/bin/fortio -- load -c $Connections -qps 0 -n $Count -loglevel Warning http://httpbin:8000/get